<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
    '/',
    function () {
        return view('welcome');
    }
);

Route::get('twitter', 'Twitter\TwitterController@getForm')
    ->name('retweet_form');

Route::post('twitter/retweet', 'Twitter\TwitterController@getRetweet')
    ->name('get_retweet');
