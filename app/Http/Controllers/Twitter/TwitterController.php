<?php

namespace App\Http\Controllers\Twitter;

use App\Http\Controllers\Controller;
use App\Twitter\TwitterResolver;
use App\Twitter\TwitterStorer;
use Illuminate\Http\Request;

class TwitterController extends Controller
{

    /**
     * @var TwitterStorer
     */
    protected $twitterStorer;

    /**
     * TwitterController constructor.
     */
    public function __construct()
    {
        $this->twitterStorer = resolve(TwitterStorer::class);
    }

    public function getForm()
    {
        return view('form');
    }

    public function getRetweet(Request $request)
    {
        $count = $this->twitterStorer->getCountRetweets($request->get('twitter_link'));

        return view('form', array('count' => $count));
    }
}
