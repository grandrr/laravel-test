<?php

namespace App\Providers;

use App\Factories\RetweetFactory;
use App\Repositories\RetweetRepository;
use App\Twitter\TwitterStorer;
use Illuminate\Support\ServiceProvider;
use App\Twitter\TwitterResolver;
use App\Twitter\J7mboTwitterProvider;

/**
 * Class TwitterServiceProvider
 * @package App\Providers
 */
class TwitterServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerTwitterResolvers();
        $this->registerRepository();
        $this->registerRetweetFactory();
        $this->registerTwitterStorer();
    }

    /**
     *
     */
    protected function registerTwitterResolvers()
    {
        $twitterProviders = array(J7mboTwitterProvider::class);

        foreach ($twitterProviders as $twitterProvider) {
            $name = 'TwitterResolver::'.$twitterProvider;
            $this->app->bind(
                $name,
                function ($app) use ($twitterProvider) {
                    $j7mboTwitterProvider = $app->make($twitterProvider);

                    return new TwitterResolver($j7mboTwitterProvider);
                }
            );
        }
    }

    /**
     *
     */
    protected function registerRepository()
    {
        $this->app->bind(
            RetweetRepository::class,
            function ($app) {
                return new RetweetRepository();
            }
        );
    }

    /**
     *
     */
    protected function registerRetweetFactory()
    {
        $this->app->bind(
            RetweetFactory::class,
            function ($app) {
                return new RetweetFactory();
            }
        );
    }

    /**
     *
     */
    protected function registerTwitterStorer()
    {
        $this->app->bind(
            TwitterStorer::class,
            function ($app) {
                $twitterResolver = $app->make('TwitterResolver::App\Twitter\J7mboTwitterProvider');
                $retweetFactory = $app->make(RetweetFactory::class);
                $retweetRepository = $app->make(RetweetRepository::class);

                return new TwitterStorer($twitterResolver, $retweetFactory, $retweetRepository);
            }
        );
    }
}
