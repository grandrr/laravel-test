<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Twitter\J7mboTwitterProvider;

class TwitterProvidersServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $oauthAccessToken = config('app.oauth_access_token');
        $oauthAccessTokenSecret = config('app.oauth_access_token_secret');
        $consumerKey = config('app.consumer_key');
        $consumerSecret = config('app.consumer_secret');

        $this->app->bind(
            J7mboTwitterProvider::class,
            function ($app) use ($oauthAccessToken, $oauthAccessTokenSecret, $consumerKey, $consumerSecret) {
                return new J7mboTwitterProvider(
                    $oauthAccessToken,
                    $oauthAccessTokenSecret,
                    $consumerKey,
                    $consumerSecret
                );
            }
        );
    }
}
