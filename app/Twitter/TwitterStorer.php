<?php

namespace App\Twitter;

use App\Factories\RetweetFactory;
use App\Repositories\RetweetRepository;
use App\Retweets;

class TwitterStorer
{

    /**
     * @var TwitterResolver
     */
    protected $twitterResolver;

    /**
     * @var RetweetFactory
     */
    protected $retweetFactory;

    /**
     * @var RetweetRepository
     */
    protected $retweetRepository;

    /**
     * TwitterStorer constructor.
     * @param TwitterResolver   $twitterResolver
     * @param RetweetFactory    $retweetFactory
     * @param RetweetRepository $retweetRepository
     */
    public function __construct(
        TwitterResolver $twitterResolver,
        RetweetFactory $retweetFactory,
        RetweetRepository $retweetRepository
    ) {
        $this->twitterResolver = $twitterResolver;
        $this->retweetFactory = $retweetFactory;
        $this->retweetRepository = $retweetRepository;
    }

    /**
     * @param string $url
     * @return int
     */
    public function getCountRetweets(string $url): int
    {
        $tweetId = $this->getTweetId($url);

        /**
         * @var $retweet Retweets
         */
        if ($retweet = $this->retweetRepository->find($tweetId)) {

            $now = new \DateTime();

            $intervalHours = $this->countRetweetIntervalHours($now, $retweet);

            if ($intervalHours >= 2) {
                $this->retweetRepository->delete($retweet);

                $retweet = $this->createRetweet($tweetId);
            }
        } else {
            $retweet = $this->createRetweet($tweetId);
        }

        return $retweet->getCount();
    }

    /**
     * @param \DateTime $date
     * @param Retweets  $retweet
     * @return string
     */
    protected function countRetweetIntervalHours(\DateTime $date, Retweets $retweet)
    {
        $dateCreated = $retweet->getDateCreated();

        $interval = $date->diff($dateCreated);

        $hours = $interval->h;

        return $hours;
    }

    /**
     * @param int $tweetId
     * @return Retweets
     */
    protected function createRetweet(int $tweetId)
    {
        $countTweets = $this->twitterResolver->getCountRetweets($tweetId);

        $retweet = $this->retweetFactory->create($tweetId, $countTweets);

        $this->retweetRepository->save($retweet);

        return $retweet;
    }

    /**
     * @param string $tweetUrl
     * @return int
     */
    protected function getTweetId(string $tweetUrl): int
    {
        $urlParts = explode('/', $tweetUrl);
        $tweetId = end($urlParts);

        return $tweetId;
    }
}
