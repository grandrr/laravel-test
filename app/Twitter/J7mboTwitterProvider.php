<?php

namespace App\Twitter;

use TwitterAPIExchange;

class J7mboTwitterProvider implements TwitterProviderInterface
{

    protected $oauthAccessToken;

    protected $oauthAccessTokenSecret;

    protected $consumerKey;

    protected $consumerSecret;

    /**
     * J7mboTwitterProvider constructor.
     * @param $oauthAccessToken
     * @param $oauthAccessTokenSecret
     * @param $consumerKey
     * @param $consumerSecret
     */
    public function __construct(
        string $oauthAccessToken,
        string $oauthAccessTokenSecret,
        string $consumerKey,
        string $consumerSecret
    ) {
        $this->oauthAccessToken = $oauthAccessToken;
        $this->oauthAccessTokenSecret = $oauthAccessTokenSecret;
        $this->consumerKey = $consumerKey;
        $this->consumerSecret = $consumerSecret;
    }

    /**
     * @param int $tweetId
     * @return int
     */
    public function getCountRetweets(int $tweetId): int
    {
        $settings = array(
            'oauth_access_token' => $this->oauthAccessToken,
            'oauth_access_token_secret' => $this->oauthAccessTokenSecret,
            'consumer_key' => $this->consumerKey,
            'consumer_secret' => $this->consumerSecret,
        );

        $retweetEndpoint = 'https://api.twitter.com/1.1/statuses/retweets/%s.json';

        $retweetEndpoint = sprintf($retweetEndpoint, $tweetId);

        $twitter = new TwitterAPIExchange($settings);
        $resultJson = $twitter->buildOauth($retweetEndpoint, 'GET')
            ->performRequest();

        $result = json_decode($resultJson);

        return count($result);
    }

}
