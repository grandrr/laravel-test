<?php

namespace App\Twitter;

class TwitterResolver
{

    /**
     * @var TwitterProviderInterface
     */
    protected $twitterProvider;

    /**
     * TwitterResolver constructor.
     * @param TwitterProviderInterface $twitterProvider
     */
    public function __construct(TwitterProviderInterface $twitterProvider)
    {
        $this->twitterProvider = $twitterProvider;
    }

    /**
     * @param int $tweetId
     * @return int
     */
    public function getCountRetweets(int $tweetId): int
    {
        return $this->twitterProvider->getCountRetweets($tweetId);
    }
}
