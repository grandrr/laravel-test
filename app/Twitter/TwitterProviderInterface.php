<?php

namespace App\Twitter;

interface TwitterProviderInterface
{
    public function getCountRetweets(int $tweetId): int;
}
