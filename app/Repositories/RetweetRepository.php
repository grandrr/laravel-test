<?php

namespace App\Repositories;

use App\Retweets;

/**
 * Class RetweetRepository
 * @package App\Repositories
 */
class RetweetRepository
{
    /**
     * @param int $tweetId
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function find(int $tweetId)
    {
        return Retweets::where('tweet_id', $tweetId)->first();
    }

    /**
     * @param Retweets $retweet
     * @return bool
     */
    public function delete(Retweets $retweet): bool
    {
        $retweet->delete();

        return true;
    }

    /**
     * @param Retweets $retweet
     * @return Retweets
     */
    public function save(Retweets $retweet)
    {
        $retweet->save();

        return $retweet;
    }
}
