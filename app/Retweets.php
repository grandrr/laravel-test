<?php

namespace App;

use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Database\Eloquent\Model;

class Retweets extends Model
{
    protected $fillable = [
        'tweet_id',
        'count',
    ];

    private $tweet_id;

    private $count;

    private $dateCreated;

    /**
     * @return int
     */
    public function getTweetId()
    {
        return $this->tweet_id;
    }

    /**
     * @param int $tweet_id
     * @return $this
     */
    public function setTweetId(int $tweet_id)
    {
        $this->tweet_id = $tweet_id;
        $this->__set('tweet_id', $tweet_id);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCount(): int
    {
        return $this->getAttribute('count');
    }

    /**
     * @param int $count
     * @return $this
     */
    public function setCount(int $count)
    {
        $this->count = $count;
        $this->__set('count', $count);

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateCreated(): Carbon
    {
        return $this->getAttribute('created_at');
    }

    /**
     * @param \DateTime $dateCreated
     * @return $this
     */
    public function setDateCreated(\DateTime $dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }
}
