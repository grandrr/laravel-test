<?php

namespace App\Factories;

use App\Retweets;

class RetweetFactory
{
    /**
     * @param $tweetId
     * @param $count
     * @return Retweets
     */
    public function create(int $tweetId, int $count): Retweets
    {
        $retweets = new Retweets();

        $retweets->setTweetId($tweetId)
            ->setCount($count);

        return $retweets;
    }
}
